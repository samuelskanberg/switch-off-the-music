# Switch off the music

A small web based geo game.

Intro: The hackers are trying to sleep at BornHack 2023 but still some party people have forgotten you can't play music at 04:00 so it is your job to switch off the music so the hackers can sleep. You have some seconds until the hackers are getting angry and starts booing.

![Screenshot](screenshot.png)

## Development

Make the index.html available:

```
python3 -m http.server 9000
```

Then visit http://localhost:9000/ with your browser

## Dependecies

* https://p5js.org

## Resources

* [Drum sound effect](https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=12918) by [Prettysleepy](https://pixabay.com/users/prettysleepy-2973588/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=12918)

Sound Effect from <a href="https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=69728">Pixabay</a>

Sound Effect by <a href="https://pixabay.com/users/soundreality-31074404/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=156454">SoundReality</a> from <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=156454">Pixabay</a>

Sound Effect from <a href="https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=33454">Pixabay</a>