const canvasWidth = 350;
const canvasHeight = 350;
const middleX = canvasWidth/2;
const middleY = canvasHeight/2;

let myInfo = {
    "name": "Alice",
    "position": {
        "latitude": 55.38707524,
        "longitude": 9.94105499
    }
};

let gameOver = false;
let score = 0;
let allPlayers = [];
let waitingForGPS = false;
let angryHackers = false;


let musicElements = [];
let booAudio;
let lastUsedMusicElementIndex = 0;

// If the user hits space to try to switch off the music
let switchOffAttempt = false;
let positionIsAvailable = false;
let updatePositionAutomatically = true;
// To show a "tail" to indicate what direction one is going
let latestPositions = [];
let hackerPatience = 100;
let timePassed = 0;
let timeStarted = 0;

function setPosition(longitude, latitude) {
    latestPositions.push({
        "longitude": myInfo.position.longitude,
        "latitude": myInfo.position.latitude
    });
    if (latestPositions.length > 10) {
        latestPositions.shift();
    }
    myInfo.position.longitude = longitude;
    myInfo.position.latitude = latitude;
}

// Get a random position that is not too far off
function getRandomPosition(refPoint) {
    const pos = {
        "latitude": refPoint.latitude - 0.0002 + Math.random()*0.0002*2,
        "longitude": refPoint.longitude - 0.0002 + Math.random()*0.0002*2,
    };
    return pos;
}

function addNewRandomPlayer() {
    console.log("Add new random player");
    lastUsedMusicElementIndex++;
    if (lastUsedMusicElementIndex >= musicElements.length) {
        lastUsedMusicElementIndex = 0;
    }
    let musicElement = musicElements[lastUsedMusicElementIndex];
    musicElement.play();
    musicElement.loop();
    const randomPosition = getRandomPosition(myInfo.position);
    allPlayers.push({
        "name": "New one",
        "position": randomPosition,
        "switched_off": false,
        // red
        "color": color(255, 100, 100),
        "music": musicElement,
        "music_started": Date.now()
    });
}

// Longitude X (increasing means going to the east). Latitude Y (increasing means going to the north)
function setInitialPosition(longitude, latitude) {
    console.log(`Got initial position: ${longitude}, ${latitude}`);
    // We shouldn't get multiple calls but if we do, we make sure we only get one
    if (positionIsAvailable) {
        return ;
    }
    setPosition(longitude, latitude);
    positionIsAvailable = true;
    // Set up the rest stuff
    gameSetup();
    // Update GPS coordinates
    setInterval(function () {
        // If GPS should update position automatically
        if (updatePositionAutomatically) {
            navigator.geolocation.getCurrentPosition(function (position) {
                console.log(`Got position ${position.coords.longitude}, ${position.coords.latitude}`);
                setPosition(position.coords.longitude, position.coords.latitude);
            });
        }
    }, 500);

    timeStarted = Date.now();
    // Update hacker patience and time passed
    setInterval(function () {
        timePassed = Date.now() - timeStarted;
    }, 1000);
}

// To skip GPS updating the coordinates
function setManual() {
    updatePositionAutomatically = false;
}

function setAuto() {
    updatePositionAutomatically = true;
}

function gameSetup() {
    console.log("Setup players");
    let musicElement = musicElements[0];
    musicElement.play();
    musicElement.loop();

    lastUsedMusicElementIndex = 0;
    // To the north
    allPlayers.push({
        "name": "Charlie",
        "position": {
            "latitude": myInfo.position.latitude + 0.0002,
            "longitude": myInfo.position.longitude
        },
        "switched_off": false,
        // red
        "color": color(255, 100, 100),
        "music": musicElement,
        "music_started": Date.now()
    });
}

function setup() {
    musicElements.push(createAudio('audio/drum-loop.mp3'));
    musicElements.push(createAudio('audio/electronic.mp3'));
    musicElements.push(createAudio('audio/hard-rock.mp3'));

    booAudio = createAudio('audio/boo.mp3');
    booAudio.loop();

    frameRate(5);
    createCanvas(canvasWidth, canvasHeight);
}

// Is running a lot of times per seconds
function draw() {
    const currentTime = Date.now();
    background(220);

    if (waitingForGPS && !positionIsAvailable) {
        textSize(24);
        textAlign(CENTER);
        text('Waiting for position...', canvasWidth/2, canvasHeight/2);
    }

    // Wait until we have our position
    if (!positionIsAvailable) {
        return ;
    }
    let doAddNewRandomPlayer = false;
    let yellowColor = color(255, 204, 0);
    let greenColor = color(100, 255, 100);
    let redColor = color(255, 100, 100);

    const playerInfo = getMyInfo();

    allPlayers.forEach(player => {
        fill(player.color);
        const diff = diffPoints(playerInfo["position"], player["position"]);
        const xDiffPixels = metersToPixels(diff["x_diff"]);
        const yDiffPixels = metersToPixels(diff["y_diff"]);
        if (switchOffAttempt) {
            if (diff.distance <= 10 && !player.switched_off) {
                player.switched_off = true;
                angryHackers = false;
                // Shut down the music and the boos
                booAudio.pause();
                player.music.pause();
                doAddNewRandomPlayer = true;
            }
        }
        if (player.switched_off) {
            fill(greenColor);
        } else {
            let timeDiff = currentTime - player.music_started;
            // It takes a while until people are annoyed with music
            if (timeDiff > 5000) {
                // Start angry crowd sound
                if (!angryHackers && !gameOver) {
                    booAudio.play();
                }
                angryHackers = true;
                hackerPatience -= 0.5;
            }
        }
        ellipse(middleX+xDiffPixels, middleY-yDiffPixels, 20, 20);
    });

    // The player "tail"
    fill(yellowColor);
    latestPositions.forEach(latestPosition => {
        const diff = diffPoints(playerInfo["position"], latestPosition);
        const xDiffPixels = metersToPixels(diff["x_diff"]);
        const yDiffPixels = metersToPixels(diff["y_diff"]);
        ellipse(middleX+xDiffPixels, middleY-yDiffPixels, 10, 10);
    });

    // Put the player in the middle
    ellipse(175,175,20,20);

    fill(0, 102, 153);
    textSize(16);
    textAlign(LEFT);
    text('Hacker patience:', 10, 15);

    let hackerPatienceWidth = Math.round((hackerPatience/100)*200);
    if (hackerPatienceWidth < 0) {
        hackerPatienceWidth = 0;
    }
    fill(0, 200, 100);
    rect(150, 5, hackerPatienceWidth, 10);

    if (hackerPatience <= 0) {
        if (!gameOver) {
            score = Math.round(timePassed/1000);
            musicElements.forEach(music => {
                music.pause();
            });
            booAudio.stop();
        }
        gameOver = true;
    }

    if (gameOver) {
        fill(240, 240, 240);
        rect(50, 100, 250, 100);
    
        fill(0, 102, 153);
        textSize(32);
        textAlign(CENTER);
        text('GAME OVER', canvasWidth/2, 150);
        fill(0, 0, 0);
        textSize(16);
        text('Score: '+score, canvasWidth/2, 170);
    } else {
        fill(0, 102, 153);
        textSize(16);
        textAlign(LEFT);
        text('Time passed: '+Math.round(timePassed/1000), 10, 30);
    }

    if (doAddNewRandomPlayer) {
        addNewRandomPlayer();
    }
    // Reset
    switchOffAttempt = false;
}

function makeSwitchOffAttempt() {
    switchOffAttempt = true;
}

function keyPressed() {
    if (keyCode === UP_ARROW) {
        // setPosition(myInfo.position.longitude, myInfo.position.latitude - 0.00001);
        setPosition(myInfo.position.longitude, myInfo.position.latitude + 0.00001);
    } else if (keyCode === DOWN_ARROW) {
        // setPosition(myInfo.position.longitude, myInfo.position.latitude + 0.00001);
        setPosition(myInfo.position.longitude, myInfo.position.latitude - 0.00001);
    } else if (keyCode === LEFT_ARROW) {
        // setPosition(myInfo.position.longitude - 0.00001, myInfo.position.latitude);
        setPosition(myInfo.position.longitude - 0.00001, myInfo.position.latitude);
    } else if (keyCode === RIGHT_ARROW) {
        // setPosition(myInfo.position.longitude + 0.00001, myInfo.position.latitude);
        setPosition(myInfo.position.longitude + 0.00001, myInfo.position.latitude);
    } else if (keyCode === 32) {
        makeSwitchOffAttempt();
    }
}

function metersToPixels(meters) {
    return meters*5;
}

function asRadians(degrees) {
    let pi = 3.14159;
    return degrees * pi / 180
}

// From https://stackoverflow.com/questions/3024404/transform-longitude-latitude-into-meters/3024728#3024728
function diffPoints(relativeNullPoint, p) {
    let deltaLatitude = p["latitude"] - relativeNullPoint["latitude"];
    let deltaLongitude = p["longitude"] - relativeNullPoint["longitude"];
    let latitudeCircumference = 40075160 * Math.cos(asRadians(relativeNullPoint["latitude"]));
    resultX = deltaLongitude * latitudeCircumference / 360;
    resultY = deltaLatitude * 40008000 / 360;
    let distance = Math.sqrt(Math.pow(resultX, 2)+Math.pow(resultY, 2));
    return {
        "x_diff": resultX,
        "y_diff": resultY,
        "distance": distance
    }
}
 
function getMyInfo() {
  return myInfo;
}
